## Throwing Game
A n-length array is divided into `n / k [+1]` parts, where k is the length of each part. These parts are sequentially labelled from 1 to `n / k [+1]`.

An example with n = 10, k = 3:
```
|---+---+---+---+---+---+---+---+---+----|
| 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 |   n = 10
|---+---+---+---+---+---+---+---+---+----|
<-----1-----><----2-----><----3-----><-4->   k = 3
```

Player has n balls:

* k balls label 1
* k balls label 2
* ...
* k balls label n/k
* `n % k` balls label `n / k + 1 (if n % k != 0)`

Player throws n balls with the goal of targeting the balls at the same label parts.

Player wins the game if every part has at least one ball with the same label.

## Problem
For a specific n, with which k does player have 50% of chance to win the game?

There are two scenarios to be considered:

1. When a ball occupies a position, other balls cannot replace it.
2. Remove the ball after each throw.

## Implementation
Using binary search to find k.

Play the game `NUM_TRIAL` times to check whether k is correct or not.
### Scenario 1 (without replacing ball)
Create a permutation of n numbers (1, 2, ..., n) by using `random_shuffle` algorithm.

### Scenario 2 (with replacing ball)
Generate a random number by using `rand()`

###Note
When `n % k != 0`, the last part can be ignored if `(n % k) / k < IGNORE_RATIO`.

## Running
### Build
```
g++ [-pipe -O2 -std=c++11] [-D CANNOT_REPLACE_BALL] [-D DEBUG] [-D NUM_TRIAL=10] [-D PROB_SUCCESS=0.5] [-D IGNORE_RATIO=0.3] -o ball-throwing ball-throwing.cpp 
```
Explanation

- `-D CANNOT_REPLACE_BALL` Set the mode of the game, if this flag is defined the ball will not be replaced; otherwise the ball will be replaced
- `-D DEBUG` Show output of the game step-by-step
- `-D NUM_TRIAL=10` Set the number of running game for each k, by default `NUM_TRIAL=1000`
- `-D PROB_SUCCESS=0.5` Set the probability of winning the game, by default `PROB_SUCCESS=0.5`
- `-D IGNORE_RATIO=0.3` Set the ratio of `(n % k) / k` in which the last part of array will be ignored, by default `IGNORE_RATIO=0.3`

### Run
To run the program, you can use this command:
```
./ball-throwing n
```

For example,
```
./ball-throwing 10 # Run the game with n = 10
```

If you want to run with different n, use the below command:
```
for ((i = 10; i < 1000; i += 10)); do ./ball-throwing $i; done
```

In case you want to debug, you should use the following command to avoid overflowing terminal screen:
```
./ball-throwing 10 > output.txt
```

Output will be appended at the end of 

- `output_with_replacing.txt` when you didn't define `CANNOT_REPLACE_BALL`
- `output_without_replacing.txt` when you defined `CANNOT_REPLACE_BALL`

## Result
Using `draw-graph.py` to compare the results of two scenarios.
