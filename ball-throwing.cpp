#include <iostream>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <bitset>
#include <algorithm>
#include <vector>
#include <ctime>
#include <cmath>
using namespace std;

//#define DEBUG

#if not defined NUM_TRIAL
#define NUM_TRIAL 1000
#endif

#if not defined PROB_SUCCESS
#define PROB_SUCCESS 0.5
#endif

#if not defined IGNORE_RATIO
#define IGNORE_RATIO 0.3 // (n%k) / k < 0.3 => ignore the last part
#endif

enum {SUCCESS, FAILURE};

//--------------------------------------------------
/// check if the last part is ignored
/// @condition: (n%k)/k < IGNORE_RATIO
//--------------------------------------------------
inline bool ignoreLastPart(int n, int k) {
  if (n % k > 0 && n % k < IGNORE_RATIO * k)
    return true;
  return false;
}

//------------------------------------------------------------------------------
/// throwing all balls
/// @return true if every part contains at least one ball with the right label
///         flase otherwise
//------------------------------------------------------------------------------
inline int play(int n, int k) {
#if defined CANNOT_REPLACE_BALL
  vector<int> permutation;
  for (int i = 0; i < n; i++)
    permutation.push_back(i);
  random_shuffle(permutation.begin(), permutation.end());
#endif

  int randomNumber;
  int label = 1;
  bool hit = false;
  bool ignore = ignoreLastPart(n, k);
  for (int i = 0; i < n; i++) {
#if defined CANNOT_REPLACE_BALL
    randomNumber = permutation[i];
#else
    randomNumber = rand() % n;
#endif
#if defined DEBUG
    cout << randomNumber << " ";
#endif
    if (randomNumber >= k*(label-1) && randomNumber <= k*label-1)
      hit = true;
    if (i == label*k-1 || (ignore == false && i == n-1)) {
      if (hit == false) { // one part without the right ball => loser
#if defined DEBUG
		cout << "\nFailure: at label " << label << endl;
#endif
		return FAILURE;
      }
      label += 1;
      hit = false;
    }
  }
#if defined DEBUG
  cout << "\nSuccess" << endl;
#endif
  return SUCCESS;
}

//---------------------------------------------------------
/// Find smallest k to win at least 50% of the games
//---------------------------------------------------------
int solve(int n) {
  int min = 0;
  int max = n;

  // After running with small n (10 -> 10000), we found out that k = 2*sqrt(n)
  // => We can shrink solution space
  min = (int)sqrt(n);
  max = 3*min;

  int k, numSuccess;
  int lastSuccess = 0;
  while (min <= max) {
    k = (min + max) / 2;
    numSuccess = 0;
#if defined DEBUG
    cout << "\n========== Try with k = " << k << endl;
#endif
    for (int i = 0; i < NUM_TRIAL; i++) {
      if (play(n, k) == SUCCESS)
	numSuccess++;
    }
#if defined DEBUG
    cout << "Number of success: " << numSuccess << endl;
#endif

    if (numSuccess >= NUM_TRIAL * PROB_SUCCESS) {
      max = k-1;
      lastSuccess = k;
    }
    else
      min = k+1;
  }
  if (lastSuccess == (int)sqrt(n))
    return -1;
  return lastSuccess;
}

int main(int argc, char *argv[]) {
  srand(unsigned(time(0)));

  if (argc >= 2) {
	int n;
    istringstream iss( argv[1] );
    if (!(iss >> n)) {
	  cerr << "Usage: " << argv[0] << " n" << endl;
	  return 0;
	}
	int k = solve(n);

#if defined DEBUG
	cout << "Result k = " << k << endl;
#else
#if defined CANNOT_REPLACE_BALL
	char filename[] = "output_without_replacing.txt";
#else
	char filename[] = "output_with_replacing.txt";
#endif
	fstream fs;
	fs.open (filename, std::fstream::in | std::fstream::out | std::fstream::app);
	fs << n << ", " << k << endl;
	fs.close();
#endif
  }
  else {
	cerr << "Usage: " << argv[0] << " n" << endl;
  }
  return 0;
}
