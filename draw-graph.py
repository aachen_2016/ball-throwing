import matplotlib.pyplot as plt
import re
import math

f = open('output_with_replacing.txt', 'r')
s = f.read()
f.close()
a = re.findall('\d+', s)
n1 = [int(a[i]) for i in range(0, len(a), 2)]
k1 = [int(a[i]) for i in range(1, len(a), 2)]
k1d = [2*math.sqrt(n1[i]) for i in range(len(n1))]

f = open('output_without_replacing.txt', 'r')
s = f.read()
f.close()
a = re.findall('\d+', s)
n2 = [int(a[i]) for i in range(0, len(a), 2)]
k2 = [int(a[i]) for i in range(1, len(a), 2)]
k2d = [2*math.sqrt(n2[i]) for i in range(len(n2))]

f, (ax1, ax2) = plt.subplots(1, 2, sharey=True)
ax1.plot(n1, k1, 'b', n1, k1d, 'r')
ax1.axis([0, 5000000, 0, 6200])
ax1.grid(True)
ax1.set_title('With Replacing Ball')
ax2.plot(n2, k2, 'b', n2, k2d, 'r')
ax2.axis([0, 5000000, 0, 6200])
ax2.grid(True)
ax2.set_title('Without Replacing Ball')
plt.show()
