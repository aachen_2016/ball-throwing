import matplotlib.pyplot as plt
import random
import re

NUM_TRIAL = 1000
PROB_SUCCESS = 0.5

def test_ball_throwing(n, k):
    n_success = 0
    for j in range(NUM_TRIAL):
        hit = [0] * (n/k)
        for i in range(0, n):
            ran = random.randint(0, n-1) / k 
            if i/k == ran and ran < len(hit):
                hit[ran] = 1
        if sum(hit) == len(hit):
            n_success += 1
    #print n_success
    if n_success > NUM_TRIAL * PROB_SUCCESS:
        return True
    return False

f = open('output_with_replacing.txt', 'r')
s = f.read()
f.close()
a = re.findall('\d+', s)
n1 = [int(a[i]) for i in range(0, len(a), 2)]
k1 = [int(a[i]) for i in range(1, len(a), 2)]

f = open('output_without_replacing.txt', 'r')
s = f.read()
f.close()
a = re.findall('\d+', s)
n2 = [int(a[i]) for i in range(0, len(a), 2)]
k2 = [int(a[i]) for i in range(1, len(a), 2)]

for i in range(len(n1)):
    if test_ball_throwing(n1[i], k1[i]) == False:
        print 'n1 = %d' % n1[i]

for i in range(len(n2)):
    if test_ball_throwing(n2[i], k2[i]) == False:
        print 'n2 = %d' % n2[i]
